# vsmlist

## The Very Simple Mailing List

Vsmlist is a very simple mailing list implemented in Python. It has been written to manage small mailing lists for everybody who wants to "just" setup a mailing list for e. g. a club without having to mess around with all the overhead of e. g. Mailman, Majordomo or Sympa.

## Homepage

The project's official homepage with further information is <https://nasauber.de/opensource/vsmlist/>.
